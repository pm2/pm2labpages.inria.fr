

GEN_HTML := public/releases/index.html public/newmadeleine/index.html public/pioman/index.html public/PadicoTM/index.html public/mpibenchmark/index.html

all: show $(GEN_HTML)

clean:
	-rm $(GEN_HTML)


pm2_FILES  := $(wildcard public/releases/pm2-*.tar.gz)
pm2_LATEST := $(lastword $(sort $(pm2_FILES)))
pm2_RELEASE := $(notdir $(pm2_LATEST))

release_root := https://pm2.gitlabpages.inria.fr/releases

define gen-link
  <li><a href=\"$(release_root)/$1\"><tt>$1</tt></a></li>
endef

define gen-files
 $(sort $(notdir $(wildcard public/releases/$1-*.tar.gz)))
endef

define latest-release
$(strip $(lastword $(sort $(call gen-files,$1))))
endef

define gen-links
  <br> <b>\&raquo;\&nbsp;</b><a href=\"$(release_root)/$(call latest-release,$1)\">Latest release: <tt>$(call latest-release,$1)</tt></a><br><br><b>\&raquo;\&nbsp;</b>Release history: <ul> $(foreach f,$(call gen-files,$1),$(call gen-link,$(f))) </ul>
endef

SUBSTRING += s,@pm2_RELEASE_STRING@,$(call gen-links,pm2),g;
SUBSTRING += s,@mpibenchmark_RELEASE_STRING@,$(call gen-links,mpibenchmark),g;
SUBSTRING += s,@mpi_sync_clocks_RELEASE_STRING@,$(call gen-links,mpi_sync_clocks),g;
SUBSTRING += s,@bench_nbc_RELEASE_STRING@,$(call gen-links,bench_nbc),g;
SUBSTRING += s,@marcel_RELEASE_STRING@,$(call gen-links,marcel),g;
SUBSTRING += s,@pthreadmicro_RELEASE_STRING@,$(call gen-links,pthreadmicro),g;
SUBSTRING += s,@tbx_RELEASE_STRING@,$(call gen-links,tbx),g;
SUBSTRING += s,@RELEASE@,$(pm2_RELEASE),g

debug:
	@echo 'SUBSTRING = "$(SUBSTRING)"'

show:
	@echo "pm2_FILES   = $(pm2_FILES)"
	@echo "pm2_LATEST  = $(pm2_LATEST)"
	@echo "pm2_RELEASE = $(pm2_RELEASE)"
	@echo

public/releases/index.html: public/releases/index.html.in $(wildcard public/releases/*.tar.gz)
	sed -e "$(SUBSTRING)" \
         public/releases/index.html.in > public/releases/index.html


public/%/index.html: public/%/index.html.in $(pm2_LATEST)
	sed -e "$(SUBSTRING)" public/$*/index.html.in > public/$*/index.html
