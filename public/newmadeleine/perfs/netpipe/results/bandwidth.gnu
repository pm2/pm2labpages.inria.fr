set terminal png size 505,378
set output "../bandwidth.png"
set key top left box

set logscale x 2
set xlabel "Message size (bytes)"
set ylabel "Bandwidth (Mbps)"

set grid

set xtics ("1" 1,"4" 4,"8" 8,"16" 16,"32" 32, "64" 64, "256" 256, "1K" 1024, \
        "2K" 2048,"4K" 4096,"8K" 8192,"32K" 32768,"  64K" 65536, \
       "256K" 262144,"1M" 1048576,"2M" 2097152,"4M" 4194304, "8M" 8388608)

plot "bandwidth.out" title " MadMPI/MX " with linespoints pt 4
