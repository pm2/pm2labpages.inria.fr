set terminal png size 505,378
set output "latency.png"
set key top left box

set logscale x 2
set xlabel "Message size (bytes)"
set ylabel "Latency (usec)"

set grid

set xtics ("1" 1,"4" 4,"8" 8,"16" 16,"32" 32, "64" 64,"128" 128,"256" 256, "512" 512, "1K" 1024, \
        "2K" 2048,"4K" 4096,"8K" 8192,"16K" 16384,"32K" 32768,"64K" 65536,"128K" 131072, \
       "256K" 262144,"512K" 524288,"1M" 1048576,"2M" 2097152,"4M" 4194304, "8M" 8388608)

plot [: 16384] "latency.out" title "MadMPI/MX " with linespoints pt 4
