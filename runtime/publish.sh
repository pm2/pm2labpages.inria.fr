#! /bin/sh

SITE_DIR=/web/runtime/html/
SITE_HOST=sync.bordeaux.inria.fr

rsync -avz . ${SITE_HOST}:${SITE_DIR}

