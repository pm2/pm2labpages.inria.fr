## PM2 website

PM2 website is managed through multiple projects:

- the main website, from this project **pm2.gitlabpages.inria.fr**, is static
- documentation is extracted with doxygen from sourcecode in CI pipeline of project **pm2**
- nightly benchmarks are hosted in the project **benchmark** and updated by the `nightly-benchmark` bot authenticated through a project token
- bench_nbc results are in the project **benchnbc-results** and graphs are built by a CI pipeline. Technically, it could be integrated to the main website but their pipeline executes in ~30 minutes

The web tree layout is as follows:

| path | content |
| ------ | ------ |
| `newmadeleine/`        | NewMadeleine website       |
| `mpibenchmark/`        | MadMPI benchmark website   |
| `PadicoTM/`            | PadicoTM website |
| `pioman/`              | pioman website |
| `bench_nbc/`           | BenchNBC website |
| `pm2/nmad/doc/`        | NewMadeleine documentation |
| `pm2/PadicoTM/doc/`    | PadicoTM documentation |
| `pm2/pioman/doc/`      | pioman documentation |
| `benchmark/nmad/`      | NewMadeleine nightly benchmark |
| `benchmark/PadicoTM/`  | PadicoTM nightly benchmark |
| `benchnbc-results/`    | Results for BenchNBC |

